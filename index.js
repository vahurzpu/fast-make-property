function quote(s) {
    return '"' + s + '"'
}

function updateExamples() {
    let exampleDiv = document.getElementById('examples')
    const desiredCount = Number(document.getElementById('example-count').value)
    const currentCount = exampleDiv.childElementCount
    if (desiredCount > currentCount) {
        for (let i = currentCount; i < desiredCount; i++) {
            let newLabel = document.createElement('label')
            newLabel.appendChild(document.createTextNode(`Example ${i + 1}: `))
            newLabel.appendChild(document.createElement('input'))
            newLabel.appendChild(document.createTextNode(' → '))
            newLabel.appendChild(document.createElement('input'))
            exampleDiv.appendChild(newLabel)
        }
    } else {
        for (let i = desiredCount; i < currentCount; i++) {
            exampleDiv.removeChild(exampleDiv.lastChild)
        }
    }
}

function examplesToQS(propID) {
    let exampleDiv = document.getElementById('examples')
    let qs = []
    for (let exampleGroup of exampleDiv.children) {
        const subject = exampleGroup.firstElementChild.value.trim()
        const object = quote(exampleGroup.lastElementChild.value).trim()
        if (subject && object) {
            qs.push([propID, 'P1855', subject, propID, object])
            qs.push([subject, propID, object])
        }
    }
    return qs
}

function parseQID(qidText) {
    let match = qidText.match(/^Q(\d+)|{{Q\|Q?(\d+)}}$/)
    if (match) {
        return 'Q' + match.slice(1).join('')
    } else {
        return ''
    }
}

function stripExternalLink(text) {
    return text.replace(/\[[^ ]+ ([^\]]+)]/, '$1')
}

function fillExamples(basicMapping) {
    const exampleCount = Object.keys(basicMapping).filter(x => x.startsWith('example ')).length
    document.getElementById('example-count').value = exampleCount
    updateExamples()
    let exampleDiv = document.getElementById('examples')
    for (let i = 0; i < exampleCount; i++) {
        const wikitext = basicMapping['example ' + (i + 1).toString()].wt
        const [_a, qidNum, _b, identifier] = wikitext.match(/{{Q\|Q?(\d+)}}\s*(→|->)\s*\[[^ ]+ ([^\]]+)\]/)
        exampleDiv.children[i].firstElementChild.value = 'Q' + qidNum
        exampleDiv.children[i].lastElementChild.value = identifier
    }
}

function fillDiscussion(dom) {
    const userTalkPrefix = 'http://www.wikidata.org/wiki/User_talk:'
    const userTalkLinks = Array.from(dom.querySelectorAll('a')).map(x => x.href).filter(x => x.startsWith(userTalkPrefix))
    const distinctUsers = Array.from(new Set(userTalkLinks)).map(x => decodeURIComponent(x.slice(userTalkPrefix.length).replace('_', ' ')))
    const discussionPost = `{{ping|${distinctUsers.join('|')}}} {{done}} ~~~~`
    document.getElementById('discussion-post').value = discussionPost
}

function fillTalkLink() {
    const propID = document.getElementById('property-id').value
    params = new URLSearchParams({
        action: 'edit',
        preload: 'User:Vahurzpu/new_property_page',
        summary: 'Create talk page'
    })
    document.getElementById('create-talk-link').href = `https://www.wikidata.org/wiki/Property_talk:${propID}?${params}`
}

window.addEventListener('DOMContentLoaded', () => {
    document.getElementById('example-count').addEventListener('change', updateExamples)
    updateExamples()


    document.getElementById('autofill').addEventListener('click', e => {
        e.preventDefault()
        const baseURL = "https://www.wikidata.org/api/rest_v1/page/html/Wikidata:Property_proposal%2F"
        const url = baseURL + encodeURIComponent(document.getElementById('proposal-name').value.replace(/ /g, '_'))
        fetch(url).then(r => r.text()).then(text => {
            let parser = new DOMParser()
            return parser.parseFromString(text, 'text/html')
        }).then(dom => {
            const templateData = JSON.parse(dom.querySelector('h2').nextElementSibling.dataset.mw)
            const basicMapping = templateData.parts[0].template.params

            function simpleFill(param, docID, mapper) {
                if (param in basicMapping) {
                    let val = basicMapping[param].wt
                    if (val && mapper) {
                        val = mapper(val)
                    }
                    if (val) {
                        document.getElementById(docID).value = val
                    }
                }
            }

            simpleFill('subject item', 'associated-wikidata', parseQID)
            simpleFill('applicable stated in value', 'stated-in-value', parseQID)
            simpleFill('allowed values', 'format-regex')
            simpleFill('source', 'source')
            simpleFill('formatter URL', 'formatter')
            simpleFill('expected completeness', 'expected-completeness', parseQID)
            simpleFill('number of ids', 'source-id-count', stripExternalLink)

            fillDiscussion(dom)
            fillTalkLink()
            fillExamples(basicMapping)
        })
    })
    document.getElementById('convert').addEventListener('click', e => {
        e.preventDefault()
        const propID = document.getElementById('property-id').value

        const assocWD = document.getElementById('associated-wikidata').value
        const statedIn = document.getElementById('stated-in-value').value
        const domains = document.getElementById('domains').value.split(',').map(x => x.trim()).filter(x => x)
        const regex = document.getElementById('format-regex').value
        const sourceURL = document.getElementById('source').value
        const urlFormatter = document.getElementById('formatter').value
        const seeAlsos = document.getElementById('see-also').value.split(',').map(x => x.trim()).filter(x => x)
        const proposalName = document.getElementById('proposal-name').value
        const expectedCompleteness = document.getElementById('expected-completeness').value
        const sourceIdCount = document.getElementById('source-id-count').value

        let qs = []

        if (assocWD) {
            qs.push([propID, 'P1629', assocWD])
        }
        if (statedIn) {
            qs.push([propID, 'P9073', statedIn])
        }
        if (domains.length > 0) {
            let typeConstraint = [propID, 'P2302', 'Q21503250']
            for (let domain of domains) {
                typeConstraint.push('P2308')
                typeConstraint.push(domain)
            }
            typeConstraint.push('P2309')
            typeConstraint.push('Q21503252')
            qs.push(typeConstraint)
        }
        if (regex) {
            qs.push([propID, 'P1793', quote(regex)])
            qs.push([propID, 'P2302', 'Q21502404', 'P1793', quote(regex)])
        }
        if (sourceURL) {
            qs.push([propID, 'P1896', quote(sourceURL)])

        }
        if (urlFormatter) {
            qs.push([propID, 'P1630', quote(urlFormatter)])
        }

        if (expectedCompleteness) {
            qs.push([propID, 'P2429', expectedCompleteness])
        }

        if (sourceIdCount && Number.isFinite(Number(sourceIdCount))) {
            qs.push([propID, 'P4876', sourceIdCount])
        }

        qs = qs.concat(examplesToQS(propID))
        if (document.getElementById('single-value').checked) {
            qs.push([propID, 'P2302', 'Q19474404'])
        }
        if (document.getElementById('distinct-value').checked) {
            qs.push([propID, 'P2302', 'Q21502410'])
        }
        if (document.getElementById('usual-property-scope').checked) {
            qs.push([propID, 'P2302', 'Q53869507', 'P5314', 'Q54828448', 'P5314', 'Q54828450'])
        }
        if (document.getElementById('usual-allowed-entity-types').checked) {
            qs.push([propID, 'P2302', 'Q52004125', 'P2305', 'Q29934200'])
        }
        qs.push([propID, 'P3254', quote('https://www.wikidata.org/wiki/Wikidata:Property_proposal/' + proposalName.replace(/ /g, '_'))])
        for (let seeAlso of seeAlsos) {
            qs.push([propID, 'P1659', seeAlso])
        }
        document.getElementById('quickstatements-code').value = qs.map(x => x.join('\t')).join("\n")
    })
})